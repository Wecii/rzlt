const Sequelize = require('sequelize');
const sequelize = require('../db');
const Model = Sequelize.Model;

const uuid = require('uuid/v4');

module.exports = (sequelize, Sequelize) => {

    var category = sequelize.define('category', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true
        },
        title: {
            type: Sequelize.TEXT + ' CHARSET utf8 COLLATE utf8_turkish_ci',
            allowNull: false,
            field: 'title'
        },
        parent:{
            type:Sequelize.UUID,
            field:'parent'
        },
        permaId: {
            type: Sequelize.STRING,
            allowNull: false,
            field: 'perma_id',
            unique: true
        },
        permalink: {
            type: Sequelize.STRING,
            allowNull: false,
            field: 'permalink'
        },
        updatedBy: {
            type: Sequelize.UUID,
            allowNull: true,
            field: 'updated_by'
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            default: true,
            allowNull: false,
            field: 'active'
        },
        createdAt:{
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            field:'created_at'
        },
        updatedAt:{
            type:Sequelize.DATE,
            field:'updated_at'
        }
    });
    return category;
};