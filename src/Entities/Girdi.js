const Sequelize = require('sequelize');
const sequelize = require('../db');
const Model = Sequelize.Model;

module.exports = (sequelize, Sequelize) => {
    var girdi = sequelize.define('girdi', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true
        },
        baslik: {
            type: Sequelize.UUID,
            allowNull: false,
            field: 'baslik_id'
        },
        author: {
            type: Sequelize.UUID,
            allowNull: false,
            field: 'created_by'
        },
        text: {
            type: Sequelize.TEXT + ' CHARSET utf8 COLLATE utf8_turkish_ci',
            allowNull: false,
            field: 'text'
        },
        permaId: {
            type: Sequelize.STRING,
            allowNull: false,
            field: 'perma_id'
        },
        permalink: {
            type: Sequelize.STRING,
            allowNull: false,
            field: 'permalink'
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            field:'created_at'
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            default: true,
            allowNull: false,
            field: 'active'
        },
        updatedBy: {
            type: Sequelize.UUID,
            allowNull: true,
            field: 'updated_by'
        }
    }, {
        get: {
            fullName() {
                return this.getDataValue('title');
            },
            permaId() {
                return this.getDataValue('permaId');
            }
            /** Other Getters */
        },
        set: {
            title(title) {
                this.setDataValue('title', title);
            },
            /** Other Setters */
        }
    });
    return girdi;
};