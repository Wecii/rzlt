const Sequelize = require('sequelize');
const sequelize = require('../db');
const Model = Sequelize.Model;

const uuid = require('uuid/v4');

module.exports = (sequelize, Sequelize) => {
    var Baslik = sequelize.define('baslik', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true
        },
        title: {
            type: Sequelize.TEXT + ' CHARSET utf8 COLLATE utf8_turkish_ci',
            allowNull: false,
            field: 'title'
        },
        slug: {
            type: Sequelize.TEXT,
            allowNull: false,
            field: 'slug'
        },
        permaId: {
            type: Sequelize.STRING,
            allowNull: false,
            field: 'perma_id',
            unique: true
        },
        permalink: {
            type: Sequelize.STRING,
            allowNull: false,
            field: 'permalink'
        },
        author: {
            type: Sequelize.UUID,
            allowNull: false,
            field: 'created_by'
        },
        updatedBy: {
            type: Sequelize.UUID,
            allowNull: true,
            field: 'updated_by'
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            default: true,
            allowNull: false,
            field: 'active'
        },
        createdAt:{
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            field:'created_at'
        },
        updatedAt:{
            type:Sequelize.DATE,
            field:'updated_at'
        }
    }, {
        get: {
            fullName() {
                return this.getDataValue('title');
            },
            id() {
                return this.getDataValue('id');
            },
            permaId() {
                return this.getDataValue('permaId');
            }
            /** Other Getters */
        },
        set: {
            title(title) {
                this.setDataValue('title', title);
            },
            /** Other Setters */
        }
    });
    return Baslik;
};