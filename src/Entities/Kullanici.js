const Sequelize = require('sequelize');
const sequelize = require('../db');
const Model = Sequelize.Model;

const uuid = require('uuid/v4');

module.exports = (sequelize, Sequelize) => {
    var Kullanici = sequelize.define('kullanici', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true
        },
        mail: {
            type: Sequelize.TEXT,
            allowNull: false,
            field: 'mail',
            validate: {
                isEmail: true,
                max: 100
            }
        },
        phone: {
            type: Sequelize.TEXT,
            allowNull: false,
            field: 'phone',
            validate: {
                isNumeric: true
            }
        },
        userName: {
            type: Sequelize.TEXT + ' CHARSET utf8 COLLATE utf8_turkish_ci',
            allowNull: false,
            field: 'user_name',
            validate: {
                min: 3,
                max: 50
            }
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
            field: 'password'
        },
        description: {
            type: Sequelize.TEXT + ' CHARSET utf8 COLLATE utf8_turkish_ci',
            allowNull: true,
            field: 'desc'
        },
        permaId: {
            type: Sequelize.STRING,
            allowNull: false,
            field: 'perma_id'
        },
        permalink: {
            type: Sequelize.STRING,
            allowNull: false,
            field: 'permalink'
        },
        updatedBy: {
            type: Sequelize.UUID,
            allowNull: true,
            field: 'updated_by'
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            defaultValue: 1,
            allowNull: false,
            field: 'active'
        },
        createdAt:{
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            field:'created_at'
        }
    }, {
        get: {
            fullName() {
                return this.getDataValue('title');
            },
            permaId() {
                return this.getDataValue('permaId');
            }
            /** Other Getters */
        },
        set: {
            title(title) {
                this.setDataValue('title', title);
            },
            /** Other Setters */
        }
    });
    return Kullanici;
};