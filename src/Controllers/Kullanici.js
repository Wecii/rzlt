/**
 * Controller that manages requests of Kullanici Model
 */


// Requirements
const kullaniciService = require("../Services/Kullanici");
const express = require('express')
const router = express.Router();


// Middleware
router.use(function timeLog(req, res, next) {
    let sess = req.session;
    console.log(sess);
    console.log('BaslikController Request Catched : ', new Date(Date.now()))
    next()
});


/** ROUTES */

// Sample GET
router.get("/", (req, res) => {
    res.json({
        "route": "Baslik Main Path GET"
    });
});

// Creating Process - POST
router.post("/", kullaniciService.create);

// Fetching Details of Baslik - GET 
router.get("/:kullaniciId", kullaniciService.findOne);

// Updating Model - PUT
router.put("/", kullaniciService.update);

// Deleting Process - DELETE
router.delete("/:kullaniciId", kullaniciService.disable);

// Disabling Process - PATCH
router.patch("/:kullaniciId", kullaniciService.disable);

module.exports = router;