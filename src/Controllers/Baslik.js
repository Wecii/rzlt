/**
 * Controller that manages requests of Baslik Model
 */


// Requirements
const baslikService = require("../Services/Baslik");
const express = require('express')
const router = express.Router();


// Middleware
router.use(function timeLog(req, res, next) {
    let sess = req.session;
    console.log(sess);
    console.log('BaslikController Request Catched : ', new Date(Date.now()))
    next()
});


/** ROUTES */

// Sample GET
router.get("/", (req, res) => {
    res.json({
        "route": "Baslik Main Path GET"
    });
});

// Creating Process - POST
router.post("/", baslikService.create);

// Fetching Details of Baslik - GET 
router.get("/:baslikId", baslikService.findOne);

// Updating Model - PUT
router.put("/", baslikService.update);

// Deleting Process - DELETE
router.delete("/:baslikid", baslikService.disable);

// Disabling Process - PATCH
router.patch("/:baslikid", baslikService.disable);

module.exports = router;