/**
 * Controller that manages requests of Girdi Model
 */


// Requirements
const girdiService = require("../Services/Girdi");
const express = require('express')
const router = express.Router();


// Middleware
router.use(function timeLog(req, res, next) {
    let sess = req.session;
    console.log(sess);
    console.log('BaslikController Request Catched : ', new Date(Date.now()))
    next()
});


/** ROUTES */

// Sample GET
router.get("/", (req, res) => {
    res.json({
        "route": "Baslik Main Path GET"
    });
});

// Creating Process - POST
router.post("/", girdiService.create);

// Fetching Details of Baslik - GET 
router.get("/:girdiId", girdiService.findOne);

// Updating Model - PUT
router.put("/", girdiService.update);

// Deleting Process - DELETE
router.delete("/:girdiId", girdiService.disable);

// Disabling Process - PATCH
router.patch("/:girdiId", girdiService.disable);

module.exports = router;