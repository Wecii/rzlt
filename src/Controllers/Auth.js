/**
 * Controller that manages requests of Authentication
 */


// Requirements
const express = require('express');
const router = express.Router();
const passport = require('passport');


/** ROUTES */

// Sample GET
router.post("/login", passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}), (req, res) => {
    try {
        return homeService.prepareHome(req, res);
    } catch (err) {
        res.json({
            err: err
        });
    }
});