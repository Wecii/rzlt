/**
 * Controller that manages requests of Home Page
 */


// Requirements
const express = require('express');
const router = express.Router();
const baslikService = require("../Services/Baslik");
const homeService = require('../Services/Home');

// Middleware
router.use(function timeLog(req, res, next) {
    let sess = req.session;
    console.log('HomeController Request Catched : ', new Date(Date.now()));
    next();
});


/** ROUTES */

// Sample GET
router.get("/", (req, res) => {
    try {
        return homeService.prepareHome(req, res);
    } catch (err) {
        res.json({
            err: err
        });
    }
});

module.exports = router;