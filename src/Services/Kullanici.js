/**
 * DB and Models
 */
const db = require('../db');
const kullanici = db.kullanici;

exports.create = (req, res) => {
    if (!req.body.text) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }
    kullanici.create(res.body).then(kullanici => {
        res.json(kullanici);
    });
};

exports.findOne = (req, res) => {
    kullanici.findAll({
        where: {
            id: req.body.id
        }
    }).then(kullanici => {
        res.json(kullanici);
    }).catch(err => {
        res.status(500).json({
            "err": err
        })
    });
};

exports.update = (req, res) => {
    kullanici.update(req.body, {
        where: {
            id: req.body.id
        }
    }).then(kullanici => {
        res.json(kullanici);
    }).catch(err => {
        res.status(500).json({
            "err": err
        });
    });
};

exports.disable = (req, res) => {
    kullanici.update({
        isActive: false
    }, {
        where: {
            id: req.body.id
        }
    }).then(kullanici => {
        res.json(kullanici);
    }).catch(err => {
        res.status(500).json({
            "err": err
        });
    });
};

exports.enable = (req, res) => {
    kullanici.update({
        isActive: true
    }, {
        where: {
            id: req.body.id
        }
    }).then(kullanici => {
        res.json(kullanici);
    }).catch(err => {
        res.status(500).json({
            "err": err
        });
    });
};