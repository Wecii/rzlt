/**
 * DB and Models
 */
const db = require('../db');
const girdi = db.girdi;

exports.create = (req, res) => {
    if (!req.body.text) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }
    girdi.create(res.body).then(girdi => {
        res.json(girdi);
    });
};

exports.findOne = (req, res) => {
    girdi.findAll({
        where: {
            id: req.body.id
        }
    }).then(girdi => {
        res.json(girdi);
    }).catch(err => {
        res.status(500).json({
            "err": err
        })
    });
};

exports.update = (req, res) => {
    girdi.update(req.body, {
        where: {
            id: req.body.id
        }
    }).then(girdi => {
        res.json(girdi);
    }).catch(err => {
        res.status(500).json({
            "err": err
        });
    });
};

exports.disable = (req, res) => {
    girdi.update({
        isActive: false
    }, {
        where: {
            id: req.body.id
        }
    }).then(girdi => {
        res.json(girdi);
    }).catch(err => {
        res.status(500).json({
            "err": err
        });
    });
};

exports.enable = (req, res) => {
    girdi.update({
        isActive: true
    }, {
        where: {
            id: req.body.id
        }
    }).then(girdi => {
        res.json(girdi);
    }).catch(err => {
        res.status(500).json({
            "err": err
        });
    });
};