/**
 * DB and Models
 */
const db = require('../db');
const baslik = db.baslik;

exports.create = (req, res) => {
    if (!req.body.text) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }
    baslik.create(res.body).then(baslik => {
        res.json(baslik);
    });
};

exports.findOne = (req, res) => {
    baslik.findAll({
        where: {
            id: req.body.id
        }
    }).then(baslik => {
        res.json(baslik);
    }).catch(err => {
        res.status(500).json({
            "err": err
        })
    });
};

exports.update = (req, res) => {
    baslik.update(req.body, {
        where: {
            id: req.body.id
        }
    }).then(baslik => {
        res.json(baslik);
    }).catch(err => {
        res.status(500).json({
            "err": err
        });
    });
};

exports.disable = (req, res) => {
    baslik.update({
        isActive: false
    }, {
        where: {
            id: req.body.id
        }
    }).then(baslik => {
        res.json(baslik);
    }).catch(err => {
        res.status(500).json({
            "err": err
        });
    });
};

exports.enable = (req, res) => {
    baslik.update({
        isActive: true
    }, {
        where: {
            id: req.body.id
        }
    }).then(baslik => {
        res.json(baslik);
    }).catch(err => {
        res.status(500).json({
            "err": err
        });
    });
};