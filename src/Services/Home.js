/**
 * DB and Models
 */
const moment = require('moment');

const db = require('../db');
const baslik = db.baslik;
const girdi = db.girdi;
const kullanici = db.kullanici;
const op = db.op;

exports.prepareHome = async (req, res) => {

    //Define all variables
    var basliks, girdis, content;
    var baslikQuery, girdiQuery;

    //Get the beginning of yesterday
    const yesterday = moment().subtract(1, 'days').startOf('day').format();
    content = {};

    //Getting basliks
    baslikQuery = 'SELECT title, permalink FROM rzlt.basliks AS baslik WHERE baslik.created_at >= "' + yesterday + '" ORDER BY created_at DESC LIMIT 50;';

    basliks = await db.sequelize.query(baslikQuery), {
        type: db.Sequelize.QueryTypes.SELECT
    };

    //Getting girdis
    girdiQuery = 'SELECT girdis.id, girdis.permalink, girdis.text, girdis.created_at as date, girdis.created_by as author, basliks.title, basliks.permalink as baslik_link FROM girdis left join basliks on girdis.baslik_id=basliks.id group by basliks.title ORDER BY RAND() LIMIT 15;';

    girdis = await db.sequelize.query(girdiQuery), {
        type: db.Sequelize.QueryTypes.SELECT
    };

    console.log(basliks[0]);
    console.log(girdis[0]);
    content.basliks = basliks[0];
    content.girdis = girdis[0];

    return res.render('home', {
        data: JSON.stringify(content)
    });

}