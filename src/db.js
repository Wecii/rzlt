const Sequelize = require('sequelize');
const sequelize = new Sequelize('rzlt', 'rzlt', '155', {
  host: 'localhost',
  dialect: 'mysql',
  define: {
    charset: 'utf8',
    collate: 'utf8_turkish_ci'
  }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

var db = {};
db.sequelize = sequelize; //contain a settings of database
db.Sequelize = Sequelize;
db.op = Sequelize.Op;

db.baslik = sequelize.import('./Entities/Baslik');
db.baslik.sync();

db.girdi = sequelize.import('./Entities/Girdi');
db.girdi.sync();

db.kullanici = sequelize.import('./Entities/Kullanici');
db.kullanici.sync();

db.tag = sequelize.import('./Entities/Tag');
db.tag.sync();

db.category = sequelize.import('./Entities/Category');
db.category.sync();

module.exports = db;