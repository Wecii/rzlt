module.exports = class JsonResponse {
    constructor(status, message, data, errors) {
        this.status = status;
        this.message = message;
        this.data = data;
        this.errors = errors;
    }
    constructor(status, message) {
        this.status = status;
        this.message = message;
    }
    constructor(status, message, data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    getStatus() {
        return this.status;
    }

    getMessage() {
        return this.message;
    }

    print() {
        console.log("Status : " + this.status + " -- Message : " + this.message + " -- ");
    }
};