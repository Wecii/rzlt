/**
 * Main Method
 */


/**
 * Main App
 */
const express = require("express");
const app = express();


/**
 * Read Environments
 */
const dotenv = require('dotenv');
dotenv.config();


/**
 * Request JSON reader Assignment
 */
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


/**
 * Session System works on Redis-Server
 */
const session = require('express-session');
const redis = require('redis');
const redisStore = require('connect-redis')(session);
const client = redis.createClient();
client.on('error', (err) => {
    console.log('Redis error: ', err);
});


/*
 * Session Middleware
 */
app.use(session({
    secret: process.env.SECRET,
    // create new redis store
    store: new redisStore({
        host: 'localhost',
        port: 6379,
        client: client,
        ttl: 260
    }),
    saveUninitialized: true,
    resave: true
}));


/**
 * Template Engine
 */
app.set('view engine', 'pug');
app.set('views', __dirname + '/../views');
app.locals.basedir = app.get('views');


/**
 * Controller Assignment
 */
app.use("/", require("./Controllers/Home"));
app.use("/entry", require("./Controllers/Girdi"));
app.use("/rzlt", require("./Controllers/Baslik"));
app.use("/user", require("./Controllers/Kullanici"));


/**
 * Static files
 */
app.use("/public", express.static(__dirname + '/../public'));


const girdi = require('./Entities/Girdi');
const baslik = require('./Entities/Baslik');
const kullanici = require('./Entities/Kullanici');
const db = require('./db');
//db.sequelize.sync();

app.listen(process.env.PORT, () =>
    console.log(`RZLT app listening on port ${process.env.PORT}!`),
);