const rp = require('request-promise');
const cheerio = require('cheerio');
const fs = require('fs');
const uuid = require('uuid/v4');

const db = require('../../db');

const baslik = db.baslik;
const girdi = db.girdi;
const kullanici = db.kullanici;


const lib = require('../../Utilities/lib');

const baseURL = 'https://eksisozluk.com';

const snooze = ms => new Promise(resolve => setTimeout(resolve, ms));

const getCompanies = async () => {
    const html = await rp(baseURL + "/basliklar/gundem");
    console.log('Html fetched from ' + baseURL + '/basliklar/gundem');
    const businessMap = cheerio('#content-body .topic-list li a', html).map(async (i, e) => {
        console.log('Mapping links. Link : ' + e.attribs.href);
        return {
            path: e.attribs.href
        };

    }).get();
    return Promise.all(businessMap);
};

const getEntries = async (result) => {
    const entryMap = result.map(async (i, e) => {

        const link = baseURL + result[e].path;

        console.log('Getting entries of ' + result[e].path);

        const innerObject = {};
        try {
            const innerHtml = await rp(link);
            innerObject.title = cheerio('#title', innerHtml).prop('data-title');
            console.log("Fetched : " + innerObject.title);
            uuidB = uuid();
            uuidP = lib.replaceAll("-", "", uuid());
            baslik.create({
                id: uuidB,
                title: innerObject.title,
                slug: lib.slugify(innerObject.title),
                permaId: uuidP,
                permalink: '/' + lib.slugify(innerObject.title) + '-' + uuidP,
                author: "agegesshg",
                isActive: 0
            }).then(ba => {
                console.log('inserted : ' + ba.title);

                innerObject.entries = [];

                cheerio('#entry-item-list li', innerHtml).each((i, item) => {

                    var o = {};
                    o.text = cheerio('.content', item).text().trim();
                    o.date = cheerio('.entry-date', item).text().trim();
                    o.author = cheerio('.entry-author', item).text().trim();

                    innerObject.entries.push(o);
                    console.log('Entry fetched. Obj : ' + o.text + ' and ' + o.date + ' and ' + o.author);

                    saveEntry(o, uuidB);

                });
            });
        } catch (err) {
            console.log(err);
            return {};
        }

    });
    return Promise.all(entryMap);
};
 
const saveEntry = async (e, baslikId) => {
    console.log("Saving Entry : "+e.text);
    uuidG = uuid();
    uuidP = lib.replaceAll("-", "", uuid());
    girdi.create({
        id: uuidG,
        baslik: baslikId,
        author: 'agsawagw',
        text: e.text.trim(),
        permaId: uuidP,
        permalink: '/entry/' + uuidP,
        isActive: 1
    }).then(gi => {
        console.log(gi.id);
    });
};

getCompanies()
    .then(result => {
        getEntries(result).then(result1 => {
                fs.writeFile("elist.json", JSON.stringify(result1), function (err) {

                    if (err) {
                        return console.log(err);
                    }

                    console.log("The file was saved!");

                });
            })
            .catch(err1 => {
                console.log(err1);
            });
    }).catch(err => {
        console.log(err);
    });