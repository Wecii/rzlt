((win, $) => {
    'use strict';

    win.rzlt = {};
    const local = {
        constants: {
            ___DEBUG: true,
            ___DEBUG_STYLE: "background:#333; color:#bada55"
        }
    };

    local.console = {
        log: (text, style) => {
            console.log('%c ' + text, style);
        },
        group: (text, style) => {
            console.group('%c ' + text, style);
        },
        debugStart: () => {
            local.console.log('Debug Process Starting'.toLocaleUpperCase(), 'color:red');
        },
        debugEnd: () => {
            local.console.log('Debug Process Ending'.toLocaleUpperCase(), 'color:red');
        },
        debug: (text) => {
            if (!local.constants.___DEBUG) {
                throw "DEBUG mode is disabled !!";
            }
            local.console.debugStart();
            local.console.log(text, local.constants.___DEBUG_STYLE);
            local.console.debugEnd();
        }
    };

    $(document).ajaxStart((req) => {
        console.log(req);
    });

    local.ajax = {
        send: {
            get: (path, options, callback, args) => {
                if (options === null) options = {
                    global: true
                };
                let req = $.ajax({
                    url: path,
                    global: options.hasOwnProperty("global") ? options.global : true
                });
                req.success((res) => {
                    local.console.log(res);
                    return callback(args);
                });
                req.error((err) => {
                    local.console.debug(err);
                });
            },
            post: (path, data, options, callback, args) => {
                if (options === null) options = {
                    global: true
                };
                let req = $.ajax({
                    url: path,
                    data: JSON.stringify(data),
                    dataType: "json",
                    contentType: "application/json;",
                    charset: "utf-8",
                    global: options.global
                });
                req.success((res) => {
                    local.console.log(res);
                    return callback(args);
                });
                req.error((err) => {
                    local.console.debug(err);
                });
            }
        }
    };

    /**
     * Main object of ajax framework
     */






    $(document).ready(function () {

        /**
         * Burger Menu
         */

        // Check for click events on the navbar burger icon
        $(".navbar-burger").click(function () {

            // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
            $(".navbar-burger").toggleClass("is-active");
            $(".navbar-menu").toggleClass("is-active");

        });

        /**
         * Üyelik Modal
         */

        jQuery('.giris').on('click', function () {
            jQuery('#giris').addClass("is-active");
        });

        jQuery('.modal-close.giris-close').on('click', function () {
            jQuery('#giris').removeClass("is-active");
        });


        /**
         * Soldaki alana başlıkların listesi
         */
        if (window.hasOwnProperty('data')) {
            var getB = function (b, s) {
                return '<div class="menu-label">\
            <a href="' + s + '" class="is-black" style="letter-spacing: 0.2px;color: #333;padding: 10px 0;position: relative;padding-right: 16px;display: block;text-transform: lowercase;font-size: 11px;">\
              ' + b + '\
              <small style="color:#888;position:absolute;right:0;top:10px;">' + (Math.floor(Math.random() * 100) + 50) + '</small>\
            </a>\
          </div>';
            };
            for (var ind in data.basliks) {
                var baslik = data.basliks[ind];
                console.log(baslik);
                jQuery('#sol-baslik-alani .menu').append(getB(baslik.title, baslik.permalink));
            }

            var getG = function (e) {
                return '<div class="entry-one" style="padding: 2px;margin-right: 0;margin-top: 20px;margin-bottom:50px;">\
                            <a href="' + e.baslik_link + '">\
                                <h1 style="color: #333;margin-bottom: 15px;text-indent: 2px;font-size: 21px;font-weight: 600;letter-spacing: 0.2px;">' + e.title + '</h1>\
                            </a>\
                            <div style="padding-left: 4px;font-size: 15px;font-weight: 400;">' + e.text + '</div>\
                            <div style="position: relative;display: block;margin-top: 19px;">\
                                <a class="entry-date permalink" href="/entry/51558350" style="color: #6666;font-size: 13px;display: block;position: absolute;width: 26%;text-align: left;text-indent: 3px;left: 11%;">' + e.date + '</a>\
                                <a class="entry-author" href="/biri/' + e.author + '" style="color: #00d1b2;font-size: 13px;display: block;position: absolute;width: 10%;text-align: left;text-indent: 3px;">entropy</a>\
                            </div>\
                        </div>\
                    ';
            };
            for (var ind1 in data.girdis) {
                var girdi = data.girdis[ind1];
                console.log(girdi);
                jQuery('#entry-alani').append(getG(girdi));
            }
        }


    });


    return;

})(window, jQuery);