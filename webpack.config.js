// webpack.config.js
var webpack = require('webpack');
module.exports = {
    mode:"development",
    entry: {
        app: [__dirname + '/public/js/main.js']
    },
    output: {
        path: __dirname + '/dist',
        filename: 'coin.bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/,
            query: {
                presets: ['@babel/preset-env']
            }
        }]
    },
    optimization: {
        minimize: true //Update this to true or false
      }
};